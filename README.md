# Sunbeam

Sunbeam is software inspired by transparency. This is the idea that information should be freely available, in the spirit of openness. Sunbeam is intended to be easy to install and configure, so website admins have to spend less time on administrative tasks.

The goal of Sunbeam is to make it easy for organizations to setup their own knowledgebases to help their users easily solve problems.
